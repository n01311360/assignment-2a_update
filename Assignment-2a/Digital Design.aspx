﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Digital Design.aspx.cs" Inherits="Assignment_2a.Webdesign" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h4>This is what I learned in CSS</h4>
    <p>In CSS padding is used to generate space around content inside of any defined borders. With CSS you would have full control over the padding. 
       There are properties for setting the padding of each side of an element (top, right, bottom, and left).</p>
    <p><strong>An example using padding:</strong></p>
    <li><ul>padding-top: 50px;</ul>
    <ul>padding-right: 30px;</ul>
    <ul>padding-bottom: 50px;</ul>
    <ul>padding-left: 80px;</ul></li> <br>

    <p><strong>Here is my example of padding:</strong></p> <br />
    <p>img {
    padding-left 10px;
    padding-bottom 5px;
    padding-right 10pc;
    { </p>
    <p>This an example of padding being used to change the spacing around an image, this is useful when you are placing an image inside of a text.</p>


    <a href="https://www.w3schools.com/css/">Learn more here</a>
</asp:Content>
<asp:Content ContentPlaceHolderID="Digital" runat="server">
</asp:Content>
